<?php

abstract class ItemController extends AbstractController 
{
    const NEWACTION_STATUS_SAVED = 1; // indica que se ha hecho un POST y que est� todo OK
    const NEWACTION_STATUS_INVALID = 2; // indica que se ha hecho POST pero que hay errores en el formulario de �tem.
    const NEWACTION_STATUS_NO_POST = 3; // indica que se ha hecho un GET.
    protected $status;
    
    protected $item;
    public function newItem() {
        $this->view->inyectJS('erasmusu.Uploader.init()');
        if (($this->view->form instanceof Erasmusu_Form_AbstractItem)){
			switch ($this->status) {
				case self::NEWACTION_STATUS_SAVED: 
					$this -> processSavedAction();
					break;               
				case self::NEWACTION_STATUS_INVALID:
					$this -> processInvalidAction();             
					break;
				case self::NEWACTION_STATUS_NO_POST:
				default:
					$this -> processNoPostAction();                
					break;
			}
		}
	}

	public function processSavedAction(){
		$this->itemTextCopyQueue($this->item);
		$this->check();
		$itemDraftId = $this->getItemDraftId();
		$photosAdded = $this->addPhotosToItem($this->item, $itemDraftId);
		$mustSave = $this->item->deleteTextTempImages();
		if ($photosAdded || $mustSave) {
			$this->mapper->save($this->item);
		}
		$this->sendContestEmail();
		$this->redirect()
	}	
	
	public function getItemDraftId(){
		return $this->view->form->item_draft_id !== null ? $this->view->form->item_draft_id->getValue(): null;
	}
	
	public function check(){
		if($this->isNewItem()){
			$this->autoCheckAndNotify($this->item);
		} else {
			$this->autoCheck($this->item);
		}
	}
	
	public function isNewItem(){
		return $this->view->form->getActionType() == 'NEW'?true:false;
	}
	
	public function redirect(){
		if ($this->item instanceof Erasmusu_Model_Item_ForumPost) {
			$url = $this->redirect;
		} elseif ($this->item instanceof Erasmusu_Model_Item_Card) {    
			$url = $this->view->link(array('controller' => 'flat','action' => 'location','values' => $this->item->getLocation()->getInterfaceUrl(),));
		} else {
			if ($this->item->hasInterface()) {
				$url = $this->view->link($this->item->getUrl(),null, array('lang' => $this->item->getLanguage()->getCode()));
			} else {
				$url = $this->view->link('warn/interface');
			}
		}
		$this->_redirect($url);
	}	
	
	public function processInvalidAction(){
		$photos = array();
		if($this->isUpdate()){
			$photos = $this->parsePhotoList($this->item->getPhotoList());
		}
		$this->redirectIfDuplicate();
		$itemDraftId = $this->getItemDraftId();
		$photos = array_merge($photos,$this->getTempPhotos($itemDraftId));
		$this->view->form->setPhotos($photos);
		$this->view->form->setItem($this->item);
	}
	
	public function isUpdate(){
		return $this->view->form->getActionType() == 'UPDATE'?true:false;
	}

	public function processNoPostAction(){
		if($this->getItemDraftId() !== null){
			$hash = md5("{$this->profileId}-review-".time());
			$this->view->form->item_draft_id->setValue($hash);
			$this->view->form->ssid->setValue(session_id());
			if($this->isUpdate() == 'UPDATE'){
				$this->view->form->setPhotos(
					$this->parsePhotoList(
						$this->item->getPhotoList()
					)
				);
			}
			$this->view->form->setItem($this->item);
		}	
	}
}
?>