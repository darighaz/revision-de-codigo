<?php
interface Figure
{
	public function getName();
	public function getPosition();
	public function rotate($degrees);
	public function jump();
	public function walk();
}

interface Mobile
{
	public function jump();
	public function walk();
}

class Person implements Figure, Mobile 
{
	protected $acceleration = 1;
	protected $color;
	protected $name;
	protected $position;
	
	public function __construct($name, $position, $color)
	{
		$this->name = $name;
		$this->position = $position;
		$this->color = $color;
	}
	
	public function jump()
	{
		$x = $position->getX();
		$x += $someWeirdNumber /* + some weird math */;
		$this->position->setX($x);
	}
	
	public function rotate($degrees)
	{
		$this->position->rotate($degrees);
	}
	public function walk()
	{
		$this->position->move($this->acceleration);
	}
	public function getName()
	{
		return $this->name;
	}
	public function setPosition($position)
	{
		$this->position = $position;
	}
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function setColor($color)
	{
		$this->color = $color;
	}
}
class Tree implements Figure
{
	protected $name;
	protected $position;
	
	public function __construct($name, $position)
	{
		$this->setName($name);
		$this->setPosition($position);
	}

	public function getName()
	{
		return $this->name;
	}
	public function setPosition($position)
	{
		$this->position = $position;
	}
	public function setName($name)
	{
		$this->name = $name;
	}
}

class Main
{
	protected $screenWidth = '800';
	protected $screenHeight = '600';
	protected $figures = array();
	
	public static function game()
	{
		$figures[] = new Person();
		$figures[] = new Tree();
		GameScreen::init($screenWidth, $screenHeight);
	}
	
	public function draw()
	{
		foreach ($this->figures as $figure) {
			if (Game::detectKeyJump()) {
				$figure->jump();
			}
			$x = $figure->getPosition()->getX();
			$y = $figure->getPosition()->getY();
			$name = $figure->getName();
			GameScreen::put($x, $y, $name);
			if ($x > GameScreen::getWith()) {
				$figure->changeColor(new RedColor());
			}
		}
	}
}

?>