<?php

class UserLoginService
{
	protected $notifications;
	
	public function execute($request)
	{
		$email = $request->getEmail();
		$password = $request->getPassword();
		$this->authenticateUser($email, $password);
	}
	
	public function authenticateUser()
	{
		$autenticated = AuthService::autenticate($email, $password);
		if ($autenticated) {
			$userRepository = new UserRepository();
			$user = $userRepository->findByEmail($email);
			$this->notifications = $user->getNotifications();
			if ($user->isBlocked()) {
				throw new AuthException('User is blocked');	
			} else {
				$this-> sendMail($email);
				Events:trigger('user.logged', array('user_id' => $user->getId()));
			}
		}else{
			throw new AuthException('User authentication fails');
		}
	}
	
	public function getNotifications()
	{
		return $this->Notifications;
	}
	
	public function sendMail($email)
	{
		$mail = new Zend_Mail();
		$mail->setTo($email);
		$mail->setBody('Sesi�n iniciada');
		$email->send();
	}
}

?>